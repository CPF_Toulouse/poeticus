# Poeticus : A propos 

Poeticus est un logiciel d'annalyse poétique écrit en JAVA par le Cabinet de Poétologie Francaise.
Poeticus permet d'un clic de souris, de scanner un mot au regard des dictionnaires présents et ainsi retrouver plus facilements les jeux de mots simple au niveau des graphèmes.  


# Poéticus : /Dictionnaires/ 

Vous trouverez ici les dictionnaires utilisés par Poeticus, libre à vous d'en créer de nouveau et de nous les envoyer par mail poetologue@gmail.com ou de les commiter ici.

Ces dictionaires répondent au formalisme `<clef> ['<donnée1>','<donnée2>'....]` et sont générés depuis **dico-words** , et utilisent des caractères d'imprimerie pour définir leur clef. 


**dico-words** : 336316 mots de la langue francaises , avec au rapport leurs type POS 

    distrait ['ADJ']
    
**dico-anagrames** : dictionnaires d'anagrames 


    &aabceln ['balance','bancale']
    
**dico-reverse** : dictionnaire des mots qui se retournent

    <ressac ['casser']


**dico-poons** : dictionnaire des mots qui changent à l'ajout de lettre 
    
    fond+e ['fondue','fondre','fondue','fondée']
  		

**dico-stars** : dictionaire des mots qui changent à l'interversion d'une lettre

	
    cor$e ['corde','corne','corse']
    
**dico-doppel** : dictionaire des mots dont deux lettres sucessives peuvent s'échanger pour changer de mot

    ba#a ['baffa','balla','banna','barra']
    ba#a#e ['baffasse','ballasse','bannasse','barrasse']